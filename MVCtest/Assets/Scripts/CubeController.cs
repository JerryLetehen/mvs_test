﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CubeController : MonoBehaviour
{
    [SerializeField] private float timeToResetModelToDefault = 10f;
    [SerializeField] private CubeModel cubeModel = null;


    private void Awake()
    {
        if (cubeModel != null)
        {
            SubcribeToModel();
        }
		StartWaitToReset();
    }

	private void StartWaitToReset()
	{
		if(waitToReset != null)
		{
			StopCoroutine(waitToReset);
		}
		waitToReset = StartCoroutine(WaitToResetModel());
	}

    private Coroutine waitToReset = null;

    private IEnumerator WaitToResetModel()
    {
		yield return new WaitForSecondsRealtime(timeToResetModelToDefault);
		OnTimeToResetEnd();
    }


	private void OnTimeToResetEnd()
	{
		cubeModel.ReseetToDefault();
		StartWaitToReset();
	}
    private void OnModelClickCountChanged()
    {
        Debug.Log("Count changes and now : " + cubeModel.ClickCount.ToString());
        cubeModel.SetCubeColor(GetRandomColor());
    }

    private void SubcribeToModel()
    {
        cubeModel.OnClickCountChanged += OnModelClickCountChanged;
    }

    private void OnDestroy()
    {
        UnsubscribeToMoodel();
    }

    private void UnsubscribeToMoodel()
    {
        cubeModel.OnClickCountChanged -= OnModelClickCountChanged;
    }

    public static Color GetRandomColor()
    {
        var r = Random.Range(0f, 1f);
        var g = Random.Range(0f, 1f);
        var b = Random.Range(0f, 1f);
        return new Color(r, g, b, 1f);
    }
}
