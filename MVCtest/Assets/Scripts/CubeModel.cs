﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CubeModel : MonoBehaviour
{
    public event Action OnClickCountChanged;

    [SerializeField] private CubeView cube = null;

    private int clickCount = default(int);
    
    public int ClickCount
    {
        get
        {
            return clickCount;
        }

        private set
        {
            clickCount = value;
            OnClickCountChanged.SafeInvoke();
        }
    }

    public Color CubeColor
    {
        get
        {
            return cube.MeshRenderer.material.color;
        }

        private set
        {
            cube.MeshRenderer.material.color = value;
        }
    }

    private void Awake()
    {
        if (cube != null)
        {
            SubscribeToCube();
        }
    }

    private void SubscribeToCube()
    {
        cube.OnMouseClick += OnMouseClickOnCube;
    }

    private void OnMouseClickOnCube()
    {
        ClickCount++;
    }


    void OnDestroy()
    {
        UnsubscribeToCube();
    }

    private void UnsubscribeToCube()
    {
        cube.OnMouseClick -= OnMouseClickOnCube;
    }

    public void ReseetToDefault()
    {
        ClickCount = 0;
        CubeColor = default(Color);
    }

    public void SetCubeColor(Color c)
    {
        CubeColor = c;
    }

}
