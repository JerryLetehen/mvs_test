﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider))]
public class CubeView : MonoBehaviour
{
    public event Action OnMouseClick;

    private Renderer meshRenderer = null;

    public Renderer MeshRenderer
    {
        get
        {
            return meshRenderer;
        }

        private set
        {
            meshRenderer = value;
        }
    }

    void Awake()
    {
        MeshRenderer = GetComponent<Renderer>();
    }

    private void OnMouseDown()
    {
        OnMouseClick.SafeInvoke();
    }
}
