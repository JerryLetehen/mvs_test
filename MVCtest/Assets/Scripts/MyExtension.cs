﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class MyExtension
{
    public static void SafeInvoke(this Action a)
    {
        if (a != null)
        {
            a.Invoke();
        }
        else
        {
            Debug.LogWarning("No one has subscribed on action you tried to invoke");
        }
    }

}